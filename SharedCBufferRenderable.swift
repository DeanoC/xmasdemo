//
//  SharedCBufferRenderable.swift
//  XmasDemo
//
//  Created by Dean Calver on 07/02/2016.
//  2018 Public Domain or whatever is closest in your country
//
//
import Foundation
import Metal
import MetalKit

class SharedCBufferRenderable : Renderable {
    
    var numInstances = 0
    var rotations:[Float]
    var positions:[float3]
    var posBuffer: MTLBuffer? = nil
    var rotBuffer: MTLBuffer? = nil
    var constantsBuffers:[MTLBuffer] = [MTLBuffer]()

    
    init( numInstances : Int, mesh:MDLMesh, mainGPU:MetalPhysicalGPU, renderPassDescriptor:MTLRenderPassDescriptor ) {
        self.numInstances = numInstances;
        rotations = [Float](repeating:0.0, count:numInstances+1)
        positions = [float3](repeating:float3(0,0,0), count:numInstances+1)
        
        super.init( mesh: mesh, mainGPU: mainGPU, renderPassDescriptor: renderPassDescriptor, uniformBufferAlloc: false )
        
        debugGroupLabel = mesh.name + " x \(self.numInstances)"
        print("\(debugGroupLabel)")
       
        let mainDevice = mainGPU.device
                
		assert(MemoryLayout<FrameUniforms>.stride == MemoryLayout<FrameUniforms>.size)
        
        let uniformBufferSize = MemoryLayout<FrameUniforms>.stride * (self.numInstances+1)
        
        for _ in 0...MaxFrameAheadBuffers {
            frameUniformBuffers.append(
                mainDevice.makeBuffer(
						length: uniformBufferSize,
						options: MTLResourceOptions.cpuCacheModeWriteCombined)! )
			
            if useComputeUpdate == true {
                constantsBuffers.append(
                    mainDevice.makeBuffer(
						length: MemoryLayout<Constants>.stride * (self.numInstances+1),
                       	options:MTLResourceOptions.cpuCacheModeWriteCombined)! )
            }
            
        }
        if useComputeUpdate == true {
			posBuffer = mainDevice.makeBuffer(
						length: MemoryLayout<float3>.stride * (self.numInstances+1),
						options:MTLResourceOptions.cpuCacheModeWriteCombined)
			rotBuffer = mainDevice.makeBuffer(
						length: MemoryLayout<Float>.stride * (self.numInstances+1),
                        options:MTLResourceOptions.cpuCacheModeWriteCombined)
            
        }
        for i in 0...numInstances {
			positions[i]=float3(Float(random_uniform(maxButNotReturned: 16))-8,
								Float(random_uniform(maxButNotReturned: 16))-8,
								Float(12+random_uniform(maxButNotReturned: 8)))
			rotations[i] = 	Float(random_uniform(maxButNotReturned: 360)) /
							Float(2.0)*Float(Float.pi)
        }

        if useComputeUpdate == true {
            let posPointer = posBuffer!.contents()
			memcpy( posPointer, positions, MemoryLayout<float3>.size * numInstances )
           
            let rotPointer = rotBuffer!.contents()
			memcpy( rotPointer, rotations, MemoryLayout<Float>.size * numInstances )
        }
        
    }
    override func setupPipelineState( renderPassDescriptor:MTLRenderPassDescriptor ) {
        let mainDevice = mainGPU.device
        
		let vertexProgram = mainGPU.defaultLibrary.makeFunction(name: "basicVertex")!
		let fragmentProgram = mainGPU.defaultLibrary.makeFunction(name: "passThroughFragment")!
        
        let pipelineStateDescriptor = MTLRenderPipelineDescriptor()
        pipelineStateDescriptor.vertexDescriptor = MTKMetalVertexDescriptorFromModelIO(mesh.vertexDescriptor)
        
        pipelineStateDescriptor.vertexFunction = vertexProgram
        pipelineStateDescriptor.fragmentFunction = fragmentProgram
        pipelineStateDescriptor.colorAttachments[0].pixelFormat = (renderPassDescriptor.colorAttachments[0].texture?.pixelFormat)!
        pipelineStateDescriptor.depthAttachmentPixelFormat = (renderPassDescriptor.depthAttachment.texture?.pixelFormat)!
        pipelineStateDescriptor.sampleCount = (renderPassDescriptor.depthAttachment.texture?.sampleCount)!
        
		try! renderPipelineState = mainDevice.makeRenderPipelineState(descriptor: pipelineStateDescriptor)
        
        if useComputeUpdate == true {
			let computeProgram = mainGPU.defaultLibrary.makeFunction(name: "transform")
			try! computePipelineState = mainDevice.makeComputePipelineState(function: computeProgram!)
        }
    }
    
    override func update(   computeEncoder : MTLComputeCommandEncoder?,
                            frameModIndex: Int,
                            viewMatrix: float4x4,
                            projectionMatrix: float4x4) {
                                
        if useComputeUpdate == true {
            do {
                let consts = constantsBuffers[frameModIndex]
                var constantData = Constants()
            
                constantData.projectionMatrix = projectionMatrix;
                constantData.viewMatrix = viewMatrix
                constantData.rotationAxis = rotationAxis
				memcpy(consts.contents(), &constantData, MemoryLayout<Constants>.size)
            }
        
            computeEncoder!.setComputePipelineState(computePipelineState!)
			computeEncoder!.setBuffer(constantsBuffers[frameModIndex],
									  	offset: 0, index: Int(kConstants.rawValue) )
            computeEncoder!.setBuffer(frameUniformBuffers[frameModIndex],
									 	offset: 0, index: Int(kFrameUniformBuffer.rawValue))
			computeEncoder!.setBuffer(posBuffer,
										offset: 0, index: Int(kPosBuffer.rawValue))
            computeEncoder!.setBuffer(rotBuffer,
										offset: 0, index: Int(kRotBuffer.rawValue))
                            
                                    
            computeEncoder!.dispatchThreadgroups(MTLSize(width: numInstances/128, height: 1, depth: 1),
                threadsPerThreadgroup: MTLSize(width: 128, height: 1, depth: 1) )
        } else
        {
            let fub = frameUniformBuffers[frameModIndex]
            
            // byte pointer by default
            let frameContentsPointer = fub.contents()
            var ptr = frameContentsPointer
            rotations.withUnsafeMutableBufferPointer{
                ( buffer: inout UnsafeMutableBufferPointer<Float>) -> () in
                for i in 0...numInstances {
                    buffer[i] += 0.02
                }
            }
            rotations.withUnsafeBufferPointer{
                (rotbuf: UnsafeBufferPointer<Float>)  in
                positions.withUnsafeMutableBufferPointer{
					( posbuf: inout UnsafeMutableBufferPointer<vector_float3>) -> () in
                    var frameData:FrameUniforms=FrameUniforms()
                    
                    for i in 0...numInstances {
						let modelMatrix =   matrix_multiply(matrixFromTranslation(pos: posbuf[i]),
                            matrixFromRotation(rotbuf[i], rotationAxis ))
                        
                        let modelViewMatrix = matrix_multiply(viewMatrix,modelMatrix)
                        let modelViewProjMatrix = matrix_multiply(projectionMatrix,modelViewMatrix)
                        frameData.projectionView = modelViewProjMatrix
                        
                        let nt = modelViewMatrix.transpose
                        let nti = nt.inverse
                        frameData.normalMatrix = nti
                        
						memcpy( ptr, &frameData, MemoryLayout<FrameUniforms>.size)
                        
						ptr = ptr + MemoryLayout<FrameUniforms>.stride
                    }
                }
            }
        }
    }
    override func draw(frameModIndex: Int,renderEncoder:MTLRenderCommandEncoder) {
        renderEncoder.pushDebugGroup(debugGroupLabel)
        renderEncoder.setRenderPipelineState(renderPipelineState!)
        renderEncoder.setDepthStencilState(depthStencilState)
		renderEncoder.setCullMode(MTLCullMode.front)
        
        var i = 0
        for vb in mesh.vertexBuffers as! [MTKMeshBuffer] {
			renderEncoder.setVertexBuffer(vb.buffer, offset:vb.offset, index:i);
            i=i+1
        }
        
        
		for sm in mesh.submeshes as! [MDLSubmesh] {
            let indexBuffer = sm.indexBuffer as! MTKMeshBuffer
			let indexType = sm.indexType.rawValue == 16 ?
													MTLIndexType.uint16 :
													MTLIndexType.uint32
            
            for i in 0...numInstances {
				renderEncoder.setVertexBuffer(  frameUniformBuffers[frameModIndex],
												offset:i*MemoryLayout<FrameUniforms>.size,
												index:Int(kFrameUniformBuffer.rawValue))

				renderEncoder.drawIndexedPrimitives(type: .triangle,
                    indexCount: sm.indexCount,
                    indexType: indexType,
                    indexBuffer: indexBuffer.buffer,
                    indexBufferOffset: 0 )
            }
        }
        
        renderEncoder.popDebugGroup()
        
    }
}
