# README #

A simple setup of Metal in Swift 2.0 that shares most of the code between iOS and OS X. Not optimised yet but already gives some idea of the CPU/GPU performance.

Can uses a tiny 16x16 render texture so often looks like doesn't do anything... But useful to eliminate fragment costs for the objects.

Instanced mode now renders all objects via one call, each with a seperate transform matrix.

Iphone 6 currently at 900,000 60 tri objects per second
Macbook pro AMD 370x at 9,000,000 60 tri objects per second
Macbook pri Intel Iris Pro at 4,000,000 60 tri objects per seconc 

Public Domain, do as you wish

### How do I get set up? ###

Clone, compile in XCode and deploy. Fire up profiler and poke around.


### Who do I talk to? ###

deano@cloudpixies.com