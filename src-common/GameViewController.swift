//
//  GameViewController.swift
//  XmasDemo
//
//  Created by Dean Calver on 22/12/15.
//  2018 Public Domain or whatever is closest in your country
//

#if os(OSX)
    import Cocoa
#else
    import UIKit
#endif

import Metal
import MetalKit
import ModelIO

let MaxFrameAheadBuffers = 3

// configuration settings
let forceLowRes = false
let forceNonSharedCBuffer = false
let useInstancing = true
let useComputeUpdate = true
#if os(OSX)
let forceLowPower = false
#endif

#if os(OSX)
let maxObjectOSX = (useInstancing ? 180000 : 30000) * 4 / (forceLowPower ? 9 : 4);
let maxObjectOSXLowRes = (useInstancing ? 178000 : 100000) * 4 / (forceLowPower ? 9 : 4);
let maxObjectsIn16ms = (forceLowRes ? maxObjectOSXLowRes : maxObjectOSX)
#else
let maxObjectIOS = (useInstancing ? 15000 : 9000);
let maxObjectIOSLowRes = (useInstancing ? 17800 : 10000);
let maxObjectsIn16ms = (forceLowRes ? maxObjectIOSLowRes : maxObjectIOS)
#endif


class CommonGameViewController
{
    var commandQueue: MTLCommandQueue! = nil
    
	let inflightSemaphore = DispatchSemaphore(value: MaxFrameAheadBuffers)
   
    var gpuGarage:GPUGarage = GPUGarage()
    var DG:MetalPhysicalGPU? = nil
    var IG:MetalPhysicalGPU? = nil
    
    var testRenderLoop:TestRenderLoop? = nil
    var initDone = false
    
    func viewDidLoad() {
        srandom(0xDEA0DEA0)
        
        #if os(iOS)
            let devices: [MTLDevice] = [ MTLCreateSystemDefaultDevice()! ]
        #else
            let devices: [MTLDevice] = MTLCopyAllDevices()
        #endif
        
        for metalDevice: MTLDevice in devices {
            #if os(iOS)
                let isMain = true
            #else
				let isMain = !metalDevice.isLowPower && !metalDevice.isHeadless
            #endif
			gpuGarage.append( gpu: MetalPhysicalGPU(device:metalDevice), isMain:isMain )
        }
        
        if gpuGarage.dGPU != nil {
            DG = gpuGarage.fastGPUs[0]
            print("main GPU: \(DG!.name)" )
        }
        if gpuGarage.iGPU != nil {
            IG = gpuGarage.lowPowerGPUs[0]
            print("low power GPU: \(IG!.name)" )
        }
        
        guard DG != nil || IG != nil else {
            print("This demo requires metal supported GPU ")
            return
        }
        
        //        guard DG != nil && IG != nil else {
        //            print("This demo requires metal support and a DG and IG (Macbook Pro with NV/AMD)")
        //            self.view = NSView(frame: self.view.frame)
        //            return
        //        }
        #if os(OSX)
        if forceLowPower == true && IG != nil {
            DG = nil;
        }
        #endif
        
        let viewGPU = (DG == nil) ? IG! : DG!
        print("TestRenderLoop using \(viewGPU.name)")
        
		commandQueue = viewGPU.device.makeCommandQueue()
        commandQueue.label = "main command queue"
        testRenderLoop = TestRenderLoop(mainGPU: viewGPU)
        testRenderLoop!.loadAssets()
        initDone = true
    }
    
    func drawInMTKView(view: MTKView) {
        if initDone == false
        {
            return
        }
        
        // use semaphore to encode 3 frames ahead
		_ = inflightSemaphore.wait(timeout: .distantFuture )

        let aspect = fabsf(Float(view.bounds.width / view.bounds.height))
        
        if let currentDrawable = view.currentDrawable
        {
            var commandsCompute : MTLCommandBuffer? = nil
            if useComputeUpdate == true {
				commandsCompute = commandQueue.makeCommandBuffer()
                commandsCompute!.label = "Compute command buffer"
                commandsCompute!.enqueue()
            }
			let commandBuffer3D = commandQueue.makeCommandBuffer()
			commandBuffer3D?.label = "3D command buffer"
			commandBuffer3D?.enqueue()
			let commandBufferBlit = commandQueue.makeCommandBuffer()
			commandBufferBlit?.label = "Blitter command buffer"
			commandBufferBlit?.enqueue()

            // use completion handler to signal the semaphore when this frame is completed allowing the encoding of the next frame to proceed
            // use capture list to avoid any retain cycles if the command buffer gets retained anywhere besides this stack frame
			commandBuffer3D?.addCompletedHandler{ [weak self] commandBuffer3D in
                if let strongSelf = self {
					strongSelf.inflightSemaphore.signal()
                }
                return
            }
            
            let srcTexture = testRenderLoop?.renderPassDescriptor.colorAttachments[0].texture!
            let dstTexture = currentDrawable.texture
            
			let blitEncoder = commandBufferBlit?.makeBlitCommandEncoder()
			blitEncoder?.label = "Final blit"
			blitEncoder?.copy(
				from: srcTexture!,
				sourceSlice: 0,
				sourceLevel: 0,
				sourceOrigin: MTLOrigin(x: 0,y: 0,z: 0),
				sourceSize: MTLSize(width: srcTexture!.width, height: srcTexture!.height,depth: 1),
				to: dstTexture,
                destinationSlice: 0,
                destinationLevel: 0,
                destinationOrigin: MTLOrigin(x: 0,y: 0,z: 0) )
            
			blitEncoder?.endEncoding()
            
			commandBufferBlit?.present(currentDrawable)
			commandBufferBlit?.commit()
			let myFrameModIndex = self.testRenderLoop!.update(commandBuffer: commandsCompute, aspectRatio: aspect)
			self.testRenderLoop!.render(commandBuffer: commandBuffer3D!, myFrameModIndex: myFrameModIndex)
        }
        
    }

}

#if os(OSX)
    class GameViewController: NSViewController, MTKViewDelegate
	{
		func draw(in view: MTKView) {
			impl.drawInMTKView(view: view)
		}
		
        var impl = CommonGameViewController();
        
        override func viewDidLoad() {
            super.viewDidLoad();
            impl.viewDidLoad();
            
            let viewGPU = (impl.DG == nil) ? impl.IG! : impl.DG!
            // setup view properties
            let view = self.view as! MTKView
            view.delegate = self
            view.device = viewGPU.device
            view.sampleCount = 1
            view.framebufferOnly = false
        }
        
        func view(view: MTKView, willLayoutWithSize size: CGSize) {
        }
		func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize){
            
        }
    }
#else
    class GameViewController: UIViewController, MTKViewDelegate
    {
        var impl = CommonGameViewController();
    
        override func viewDidLoad() {
            super.viewDidLoad();
            impl.viewDidLoad();
            
            let viewGPU = (impl.DG == nil) ? impl.IG! : impl.DG!
           // setup view properties
            let view = self.view as! MTKView
            view.delegate = self
            view.device = viewGPU.device
            view.sampleCount = 1
            view.framebufferOnly = false
        }
        
        func drawInMTKView(view: MTKView) {
            impl.drawInMTKView(view)
        }


        func view(view: MTKView, willLayoutWithSize size: CGSize) {
        }
        func mtkView(view: MTKView, drawableSizeWillChange size: CGSize){
            
        }
    }
#endif
