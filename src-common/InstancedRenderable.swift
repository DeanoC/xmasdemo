//
//  InstancedRenderable.swift
//  XmasDemo
//
//  Created by Dean Calver on 06/02/2016.
//  2018 Public Domain or whatever is closest in your country
//

import Foundation
import Metal
import MetalKit

class InstancedRenderable : SharedCBufferRenderable {
        
    override func setupPipelineState( renderPassDescriptor:MTLRenderPassDescriptor ) {
        let mainDevice = mainGPU.device
        
		let vertexProgram = mainGPU.defaultLibrary.makeFunction(name: "basicInstancedVertex")!
		let fragmentProgram = mainGPU.defaultLibrary.makeFunction(name: "passThroughFragment")!
        
        let pipelineStateDescriptor = MTLRenderPipelineDescriptor()
        pipelineStateDescriptor.vertexDescriptor = MTKMetalVertexDescriptorFromModelIO(mesh.vertexDescriptor)
        
        pipelineStateDescriptor.vertexFunction = vertexProgram
        pipelineStateDescriptor.fragmentFunction = fragmentProgram
        pipelineStateDescriptor.colorAttachments[0].pixelFormat = (renderPassDescriptor.colorAttachments[0].texture?.pixelFormat)!
        pipelineStateDescriptor.depthAttachmentPixelFormat = (renderPassDescriptor.depthAttachment.texture?.pixelFormat)!
        pipelineStateDescriptor.sampleCount = (renderPassDescriptor.depthAttachment.texture?.sampleCount)!
        
		try! renderPipelineState = mainDevice.makeRenderPipelineState(descriptor: pipelineStateDescriptor)

        if useComputeUpdate == true {

			let computeProgram = mainGPU.defaultLibrary.makeFunction(name: "transform")
			try! computePipelineState = mainDevice.makeComputePipelineState(function: computeProgram!)
        }

    }
    override func draw(frameModIndex: Int,renderEncoder:MTLRenderCommandEncoder) {
        renderEncoder.pushDebugGroup(debugGroupLabel)
        renderEncoder.setRenderPipelineState(renderPipelineState!)
        renderEncoder.setDepthStencilState(depthStencilState)
		renderEncoder.setCullMode(MTLCullMode.front)
        
        var i = 0
        for vb in mesh.vertexBuffers as! [MTKMeshBuffer] {
			renderEncoder.setVertexBuffer(vb.buffer, offset:vb.offset, index:i);
            i=i+1
        }

		renderEncoder.setVertexBuffer(frameUniformBuffers[frameModIndex], offset:0, index:3)
        
		for sm in mesh.submeshes as! [MDLSubmesh] {
            let indexBuffer = sm.indexBuffer as! MTKMeshBuffer
			let indexType = sm.indexType.rawValue == 16 ?
													MTLIndexType.uint16 :
													MTLIndexType.uint32
            
			renderEncoder.drawIndexedPrimitives(type: .triangle,
                indexCount: sm.indexCount,
                indexType: indexType,
                indexBuffer: indexBuffer.buffer,
                indexBufferOffset: 0,
                instanceCount: numInstances )
        }
        
        renderEncoder.popDebugGroup()
        
    }
}
