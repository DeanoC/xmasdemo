//
//  MathUtils.h
//  XmasDemo
//
//  Created by Dean Calver on 06/02/2016.
//  2018 Public Domain or whatever is closest in your country
//

#ifndef MathUtils_h
#define MathUtils_h


#if __METAL_VERSION__
metal::float4x4 matrixFromPerspectiveFOVAspectLH( const float fovY, const float aspect, const float nearZ, const float farZ );
metal::float4x4 matrixFromRotation( const float radians, const metal::float3 axis );
metal::float4x4 matrixFromTranslation(const metal::float3 pos );
#else

#include <simd/simd.h>


#if __cplusplus
extern "C"
#endif
 matrix_float4x4 matrixFromPerspectiveFOVAspectLH( const float fovY, const float aspect, const float nearZ, const float farZ );
#if __cplusplus
extern "C"
#endif
matrix_float4x4 matrixFromRotation( const float radians, const vector_float3 axis );

#endif

#endif /* MathUtils_h */
