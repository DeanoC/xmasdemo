//
// Created by Dean Calver on 18/12/15.
// 2018 Public Domain or whatever is closest in your country
//

import Foundation

class GPUGarage {
    var fastGPUs: [MetalPhysicalGPU] = []
    var lowPowerGPUs: [MetalPhysicalGPU] = []
    var fastHeadlessGPUs: [MetalPhysicalGPU] = []
    var lowPowerHeadlessGPUs: [MetalPhysicalGPU] = []
    
    var dGPU: MetalPhysicalGPU? = nil // the main discrete GPU if one exists
    var iGPU: MetalPhysicalGPU? = nil // the integerated GPU if it exists
    
    var otherGPUs: [MetalPhysicalGPU] = [] // any left over GPUs bar dGPU and iGPU
    
    func append(gpu: MetalPhysicalGPU, isMain : Bool = false  ){
        if gpu.lowPower == true && gpu.visible == true {
            lowPowerGPUs.append(gpu)
        } else
            if gpu.lowPower == true && gpu.visible == false {
                lowPowerHeadlessGPUs.append(gpu)
            } else
                if gpu.lowPower == false && gpu.visible == false {
                    fastHeadlessGPUs.append(gpu)
                } else
                    if gpu.lowPower == false && gpu.visible == true {
                        fastGPUs.append(gpu)
        }
        
        if( isMain == true ){
            dGPU = gpu
        } else if( gpu.lowPower ){
            iGPU = gpu
        } else {
            otherGPUs.append(gpu)
        }
    }
    
}
