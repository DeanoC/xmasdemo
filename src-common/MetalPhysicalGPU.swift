//
// Created by Dean Calver on 18/12/15.
//  2018 Public Domain or whatever is closest in your country
//

import Foundation
import Metal

class MetalPhysicalGPU {
    init(device: MTLDevice, maxCommandBuffers: Int = 10) {
        self.device = device
		self.cmdQueue = self.device.makeCommandQueue(maxCommandBufferCount: maxCommandBuffers)!
		self.defaultLibrary = self.device.makeDefaultLibrary()!
        self.cmdQueue.label = "\(self.name)'s command queue"
    }
    var name: String {
		return device.name
    }
    #if os(OSX)
    var visible: Bool {
		return !device.isHeadless
    }
    var lowPower: Bool {
		return device.isLowPower
    }
    #else
    var visible: Bool {
        return true
    }
    var lowPower: Bool {
        return false
    }
    #endif
    func display() {
    }
    func newCommandBuffer() -> MTLCommandBuffer {
        // TODO change to unretained references when proper fencing is done
		return cmdQueue.makeCommandBuffer()!
    }
    
    var device: MTLDevice
    var cmdQueue: MTLCommandQueue
    var defaultLibrary:MTLLibrary
    
}
