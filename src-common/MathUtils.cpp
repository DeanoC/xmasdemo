//
//  MathUtils.cpp
//  XmasDemo
//
//  Created by Dean Calver on 07/02/2016.
//  2018 Public Domain or whatever is closest in your country
//


#define BASE_C_VERSIONS 1

#if BASE_C_VERSIONS

#include <stdio.h>
#include <simd/simd.h>
#include "MathUtils.h"

extern "C" matrix_float4x4 matrixFromPerspectiveFOVAspectLH( const float fovY, const float aspect, const float nearZ, const float farZ )
{
    using namespace simd;
    
    const float yscale = 1.0f / tan( fovY * 0.5f );
    const float xscale = yscale / aspect;
    const float q = farZ / (farZ - nearZ);
    
    return matrix_float4x4{     vector_float4{xscale, 0,      0,          0},
                                vector_float4{0,      yscale, 0,          0},
                                vector_float4{0,      0,      q,          1},
                                vector_float4{0,      0,      q * -nearZ, 0 } };
}


// axis should be normalized
extern "C" matrix_float4x4 matrixFromRotation( const float radians, const vector_float3 v )
{
    using namespace simd;
    
#if defined __METAL_VERSION__
    float cosi;
    const float sine = sincos( radians, cosi );
#else
    const float sine = sin(radians);
    const float cosi = cos(radians);
#endif
    const float cosp = 1.0f - cosi;
    
    const float m00 = cosi + cosp * v.x * v.x;
    const float m01 = cosp * v.x * v.y + v.z * sine;
    const float m02 = cosp * v.x * v.z - v.y * sine;
    
    const float m10 = cosp * v.x * v.y - v.z * sine;
    const float m11 = cosi + cosp * v.y * v.y;
    const float m12 = cosp * v.y * v.z + v.x * sine;
    
    const float m20 = cosp * v.x * v.z + v.y * sine;
    const float m21 = cosp * v.y * v.z - v.x * sine;
    const float m22 = cosi + cosp * v.z * v.z;
    
    return float4x4{    float4{m00, m01, m02, 0},
                        float4{m10, m11, m12, 0},
                        float4{m20, m21, m22, 0},
                        float4{0,   0,   0,   1 } };
    
}

#endif
