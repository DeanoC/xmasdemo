//
//  MathUtils.metal
//  XmasDemo
//
//  Created by Dean Calver on 06/02/2016.
//  2018 Public Domain or whatever is closest in your country
//

#include <metal_stdlib>
#include "MathUtils.h"
using namespace metal;

float4x4 matrixFromPerspectiveFOVAspectLH( const float fovY, const float aspect, const float nearZ, const float farZ )
{
    const float yscale = 1.0f / tan( fovY * 0.5f );
    const float xscale = yscale / aspect;
    const float q = farZ / (farZ - nearZ);
    
    return float4x4(    float4(xscale, 0,      0,          0),
                        float4(0,      yscale, 0,          0),
                        float4(0,      0,      q,          1),
                        float4(0,      0,      q * -nearZ, 0 ) );
}

// axis should be normalized TODO remove normalization step
float4x4 matrixFromRotation( const float radians, const float3 axis )
{
    const float3 v = normalize( axis );
    float cosi;
    const float sine = sincos( radians, cosi );
    const float cosp = 1.0f - cosi;
    
    const float m00 = cosi + cosp * v.x * v.x;
    const float m01 = cosp * v.x * v.y + v.z * sine;
    const float m02 = cosp * v.x * v.z - v.y * sine;
    
    const float m10 = cosp * v.x * v.y - v.z * sine;
    const float m11 = cosi + cosp * v.y * v.y;
    const float m12 = cosp * v.y * v.z + v.x * sine;
    
    const float m20 = cosp * v.x * v.z + v.y * sine;
    const float m21 = cosp * v.y * v.z - v.x * sine;
    const float m22 = cosi + cosp * v.z * v.z;
    
    return float4x4(    float4(m00, m01, m02, 0),
                        float4(m10, m11, m12, 0),
                        float4(m20, m21, m22, 0),
                        float4(0,   0,   0,   1 ) );

}

float4x4 matrixFromTranslation(const float3 pos ) {
    return  float4x4{
        float4{1,       0,      0,      0},
        float4{0,       1,      0,      0},
        float4{0,       0,      1,      0},
        float4{pos.x,   pos.y,  pos.z,   1 }
    };
}
