//
//  Shaders.h
//  XmasDemo
//
//  Created by Dean Calver on 23/12/15.
//  2018 Public Domain or whatever is closest in your country
//

#ifndef Shaders_h
#define Shaders_h

#include <simd/simd.h>

//using namespace simd;

/// Indices for buffer bind points.
enum BufferIndex  {
    kPosBuffer = 0,
    kRotBuffer = 1,
    kConstants = 2,
    kFrameUniformBuffer    = 3,
};

/// Per frame uniforms.
typedef struct  {
//    matrix_float4x4 model;
//    matrix_float4x4 view;
//    matrix_float4x4 projection;
    matrix_float4x4 projectionView;
    matrix_float4x4 normalMatrix;

    matrix_float4x4 dummy0;
    matrix_float4x4 dummy1;

} FrameUniforms;

typedef struct {
    vector_float3 rotationAxis;
    matrix_float4x4 viewMatrix;
    matrix_float4x4 projectionMatrix;
} Constants;

#endif /* Shaders_h */
