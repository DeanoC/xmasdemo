//
//  Renderable.swift
//  XmasDemo
//
//  Created by Dean Calver on 25/12/15.
//  2018 Public Domain or whatever is closest in your country
//

import Foundation
import Metal
import MetalKit

class Renderable {    
    var mesh:MDLMesh
    var renderPipelineState : MTLRenderPipelineState? = nil
    var computePipelineState : MTLComputePipelineState? = nil
    var depthStencilState:MTLDepthStencilState
    var debugGroupLabel : String
    var frameUniformBuffers:[MTLBuffer] = [MTLBuffer]()
    var mainGPU:MetalPhysicalGPU

    let position:float3
    var rotation:Float = 0
    
    init( mesh:MDLMesh, mainGPU:MetalPhysicalGPU, renderPassDescriptor:MTLRenderPassDescriptor, uniformBufferAlloc : Bool = true ) {
        self.mesh = mesh
        self.mainGPU = mainGPU
        
        debugGroupLabel = mesh.name
        let mainDevice = mainGPU.device
        
        let depthStencilStateDescriptor = MTLDepthStencilDescriptor()
		depthStencilStateDescriptor.depthCompareFunction = MTLCompareFunction.lessEqual
		depthStencilStateDescriptor.isDepthWriteEnabled = true
        
		depthStencilState = mainDevice.makeDepthStencilState(descriptor: depthStencilStateDescriptor)!
        
        // Create a uniform buffer that we'll dynamicall update each frame.
        if( uniformBufferAlloc == true ) {
            for _ in 0...MaxFrameAheadBuffers {
				frameUniformBuffers.append( mainDevice.makeBuffer(length: MemoryLayout<FrameUniforms>.size, options:MTLResourceOptions.cpuCacheModeWriteCombined)! );
            }
        }

		position=float3( Float(random_uniform(maxButNotReturned: 16))-8, Float(random_uniform(maxButNotReturned: 16))-8, Float(12+random_uniform(maxButNotReturned: 8)))

		setupPipelineState(renderPassDescriptor: renderPassDescriptor)
  
    }
    
    func setupPipelineState( renderPassDescriptor:MTLRenderPassDescriptor ) {
        let mainDevice = mainGPU.device
        
		let vertexProgram = mainGPU.defaultLibrary.makeFunction(name: "basicVertex")!
		let fragmentProgram = mainGPU.defaultLibrary.makeFunction(name: "passThroughFragment")!
        
        let pipelineStateDescriptor = MTLRenderPipelineDescriptor()
        pipelineStateDescriptor.vertexDescriptor = MTKMetalVertexDescriptorFromModelIO(mesh.vertexDescriptor)
        
        pipelineStateDescriptor.vertexFunction = vertexProgram
        pipelineStateDescriptor.fragmentFunction = fragmentProgram
        pipelineStateDescriptor.colorAttachments[0].pixelFormat = (renderPassDescriptor.colorAttachments[0].texture?.pixelFormat)!
        pipelineStateDescriptor.depthAttachmentPixelFormat = (renderPassDescriptor.depthAttachment.texture?.pixelFormat)!
        pipelineStateDescriptor.sampleCount = (renderPassDescriptor.depthAttachment.texture?.sampleCount)!
        
		try! renderPipelineState = mainDevice.makeRenderPipelineState(descriptor: pipelineStateDescriptor)
    }

    let rotationAxis = normalize(float3(1,1,0))
  
    func update(    computeEncoder : MTLComputeCommandEncoder?,
                    frameModIndex:Int,
                    viewMatrix:float4x4,
                    projectionMatrix:float4x4) {

        rotation += 0.02
        
        let frameContentsPointer = frameUniformBuffers[frameModIndex].contents()

		var frameData:FrameUniforms=FrameUniforms()
     
		let modelMatrix =   matrix_multiply(matrixFromTranslation(pos: position),
            matrixFromRotation(rotation, rotationAxis ))
        
        let modelViewMatrix = matrix_multiply(viewMatrix,modelMatrix)
        let modelViewProjMatrix = matrix_multiply(projectionMatrix,modelViewMatrix)
        frameData.projectionView = modelViewProjMatrix
        
        let nt = modelViewMatrix.transpose
        let nti = nt.inverse
        frameData.normalMatrix = nti
        
		frameContentsPointer.storeBytes(of: frameData, as: FrameUniforms.self)
    }
    func draw(frameModIndex:Int, renderEncoder:MTLRenderCommandEncoder) {
        
        renderEncoder.pushDebugGroup(debugGroupLabel)
        renderEncoder.setRenderPipelineState(renderPipelineState!)
        renderEncoder.setDepthStencilState(depthStencilState)
		renderEncoder.setCullMode(MTLCullMode.front)
        
        var i = 0
        for vb in mesh.vertexBuffers as! [MTKMeshBuffer] {
			renderEncoder.setVertexBuffer(vb.buffer, offset:vb.offset, index:i);
            i=i+1
		}
		renderEncoder.setVertexBuffer(frameUniformBuffers[frameModIndex	], offset:0, index:3)
        
		for sm in mesh.submeshes as! [MDLSubmesh] {
            let indexBuffer = sm.indexBuffer as! MTKMeshBuffer
			let indexType = sm.indexType.rawValue == 16 ?
													MTLIndexType.uint16 :
													MTLIndexType.uint32
			renderEncoder.drawIndexedPrimitives(type: .triangle,
												indexCount: sm.indexCount,
												indexType: indexType,
												indexBuffer: indexBuffer.buffer,
												indexBufferOffset: 0 )
        }
        renderEncoder.popDebugGroup()
    }
}
