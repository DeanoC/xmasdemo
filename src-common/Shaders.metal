//
//  Shaders.metal
//  XmasDemo
//
//  Created by Dean Calver on 22/12/15.
//  2018 Public Domain or whatever is closest in your country
//

#include <metal_stdlib>
#include "MathUtils.h"
#include "Shaders.h"
#include <metal_matrix>
using namespace metal;





kernel void transform(
                device float3* posbuf [[ buffer(kPosBuffer) ]],
                device float* rotbuf [[ buffer(kRotBuffer) ]],
                device FrameUniforms* frameData [[ buffer(kFrameUniformBuffer) ]],
                constant const Constants& constants[[ buffer(kConstants)]],
                uint id [[ thread_position_in_grid ]] )
{
    rotbuf[id] = rotbuf[id] + 0.02f;
    float4x4 modelMatrix = matrixFromTranslation(posbuf[id]) *
                            matrixFromRotation(rotbuf[id], constants.rotationAxis);
    
    float4x4 modelViewMatrix = constants.viewMatrix * modelMatrix;
    float4x4 modelViewProjMatrix = constants.projectionMatrix * modelViewMatrix;
    frameData[id].projectionView = modelViewProjMatrix;

//    float4x4 nt = transpose(modelViewMatrix);
//    float4x4 nti = invert(nt);
    frameData[id].normalMatrix = modelMatrix;
            
}

struct VertexIn
{
    float3  position [[attribute(0)]];
    float3  normal [[attribute(1)]];
};

struct VertexOut
{
    float4  position [[position]];
    float4  colour;
};

vertex VertexOut basicVertex(  VertexIn in  [[ stage_in ]],
                                     const constant FrameUniforms& frameData   [[ buffer(kFrameUniformBuffer) ]]
                                     )
{
    VertexOut outVertex;
    const float4 pos = float4(in.position, 1);
    outVertex.position = frameData.projectionView * pos;
    const float d = dot((frameData.normalMatrix * float4(normalize(in.normal),0)).xyz, float3(0,1,0));
    outVertex.colour.rgb = max(d,0.0) + 0.1;
    outVertex.colour.a = 1;
    
    return outVertex;
};

vertex VertexOut basicInstancedVertex(  VertexIn in  [[ stage_in ]],
                                        const constant FrameUniforms* frameData   [[ buffer(kFrameUniformBuffer) ]],
                                        const uint iid [[instance_id]]
                                   )
{
    VertexOut outVertex;
    const float4 pos = float4(in.position, 1);
    outVertex.position = frameData[iid].projectionView * pos;
    const float d = dot((frameData[iid].normalMatrix * float4(normalize(in.normal),0)).xyz, float3(0,1,0));
    outVertex.colour.rgb = max(d,0.0) + 0.1;
    outVertex.colour.a = 1;
    
    return outVertex;
};


fragment half4 passThroughFragment(VertexOut inFrag [[stage_in]])
{
    return (half4)inFrag.colour;
};
