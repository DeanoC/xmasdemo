//
//  RenderLoop.swift
//  XmasDemo
//
//  Created by Dean Calver on 25/12/15.
//  2018 Public Domain or whatever is closest in your country
//

import Foundation
import Metal
import MetalKit

class TestRenderLoop {
    
    var mtkAlloc:MTKMeshBufferAllocator? = nil
    var mainGPU:MetalPhysicalGPU
    var frameUniformBuffers:[MTLBuffer] = [MTLBuffer]()
    var renderables:[Renderable] = [Renderable]()

    var colourRenderTarget:MTLTexture
    var depthRenderTarget:MTLTexture

    let renderPassDescriptor = MTLRenderPassDescriptor()
    
    var frameIndex:Int32=0
  
    init( mainGPU: MetalPhysicalGPU ) {
        self.mainGPU = mainGPU
        
        print( "Instanced Mode: \(useInstancing)" )
        print( "forceNonSharedCBuffer: \(forceNonSharedCBuffer)" )
        
        let mainDevice = mainGPU.device

        #if os(iOS)
            let screenSize = forceLowRes ? 	CGRect( 0,0,16, 16) :
											UIScreen.mainScreen().nativeBounds
        #else
            // TODO set the size properly on OS X
			let screenSize = forceLowRes ? 	CGRect(x:0,y:0,width:16,height:16) :
											CGRect(x:0,y:0,width:960,height:720)
        #endif

        let colourTexDesc = MTLTextureDescriptor()
        colourTexDesc.width = Int(screenSize.width)
        colourTexDesc.height = Int(screenSize.height)
		colourTexDesc.textureType = MTLTextureType.type2D
		colourTexDesc.pixelFormat = MTLPixelFormat.bgra8Unorm
        colourTexDesc.sampleCount = 1
		colourTexDesc.usage = MTLTextureUsage.renderTarget;
		colourTexDesc.resourceOptions = MTLResourceOptions.storageModePrivate
		colourRenderTarget = mainDevice.makeTexture(descriptor: colourTexDesc)!
        let depthTexDesc = MTLTextureDescriptor()
        depthTexDesc.width = Int(screenSize.width)
        depthTexDesc.height = Int(screenSize.height)
		depthTexDesc.textureType = MTLTextureType.type2D
		depthTexDesc.pixelFormat = MTLPixelFormat.depth32Float
        depthTexDesc.sampleCount = 1
		depthTexDesc.usage = MTLTextureUsage.renderTarget;
		depthTexDesc.resourceOptions = MTLResourceOptions.storageModePrivate
		depthRenderTarget = mainDevice.makeTexture(descriptor: depthTexDesc)!
        
        renderPassDescriptor.colorAttachments[0].texture = colourRenderTarget
		renderPassDescriptor.colorAttachments[0].loadAction = MTLLoadAction.clear
        renderPassDescriptor.colorAttachments[0].clearColor = MTLClearColorMake(1,0,0,1)
		renderPassDescriptor.colorAttachments[0].storeAction = MTLStoreAction.store
        renderPassDescriptor.depthAttachment.texture = depthRenderTarget
		renderPassDescriptor.depthAttachment.loadAction = MTLLoadAction.clear
        renderPassDescriptor.depthAttachment.clearDepth = 1
		renderPassDescriptor.depthAttachment.storeAction = MTLStoreAction.dontCare

    }
    
    func loadAssets() {
        let mainDevice = mainGPU.device
        
        mtkAlloc = MTKMeshBufferAllocator(device: mainDevice)
		let die20 = MDLMesh.newIcosahedron(withRadius: 1, inwardNormals: false, allocator: mtkAlloc)
        

    
        if forceNonSharedCBuffer == false {
            if useInstancing {
                renderables.append( InstancedRenderable( numInstances:maxObjectsIn16ms, mesh:die20,mainGPU: mainGPU,renderPassDescriptor: renderPassDescriptor))
            } else {
                renderables.append( SharedCBufferRenderable( numInstances:maxObjectsIn16ms, mesh:die20,mainGPU: mainGPU,renderPassDescriptor: renderPassDescriptor))
            }
        } else {
            // so slow not worth going too many object            
            for _ in 0..<2000 {
                renderables.append(Renderable(mesh:die20,mainGPU: mainGPU,renderPassDescriptor: renderPassDescriptor))
            }
        }
        
    }
    
    func update( commandBuffer:MTLCommandBuffer?, aspectRatio:Float) -> Int {
        let projectionMatrix = matrixFromPerspectiveFOVAspectLH(
						Float(65 * Float.pi / 180),aspectRatio, 0.1,100)
        
        let viewMatrix = matrixFromTranslation(x: 0, y: 0, z: 5)
        let myFrameModIndex: Int = Int(OSAtomicIncrement32(&frameIndex)) % MaxFrameAheadBuffers
        
        var computeEncoder : MTLComputeCommandEncoder? = nil
        if commandBuffer != nil {
			computeEncoder = commandBuffer!.makeComputeCommandEncoder()
            computeEncoder!.label = "compute encoder"
        }
        
        for r in renderables {
			r.update(computeEncoder: computeEncoder, frameModIndex: myFrameModIndex, viewMatrix: viewMatrix,projectionMatrix: projectionMatrix)
        }

        if commandBuffer != nil {
            computeEncoder!.endEncoding()
            commandBuffer!.commit()
        }

        return myFrameModIndex
    }
    
    func render(commandBuffer:MTLCommandBuffer, myFrameModIndex:Int ) {
		let renderEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: renderPassDescriptor)
		renderEncoder?.label = "render encoder"

        for r in renderables {
			r.draw(frameModIndex: myFrameModIndex, renderEncoder: renderEncoder!)
        }
		renderEncoder?.endEncoding()
        
        commandBuffer.commit()
    
    }

}
